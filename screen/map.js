import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput } from 'react-native';
import MapView, {Marker} from 'react-native-maps'
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';


class Map extends React.Component {
	constructor(props){
	   	super(props)
    this.state = {
      lat: 12.866667, 
      lng: 2.333333
    }	 
	}

  componentDidMount(){
    this.getGeolocAsync();
  }

  getGeolocAsync = async ()=>{
        let { status } = await Permissions.askAsync(Permissions.LOCATION);
        if (status !== 'granted') {
          setErrorMessage('Permission to access location was denied');
          return
        } 
        // récupère geoloc
        let myLocation = await Location.getCurrentPositionAsync({});
        console.log(myLocation);
        this.setState({lat: myLocation.coords.latitude, lng: myLocation.coords.longitude})
  }


	render(){
		return (
			<View style={styles.container}>
        <View style={{flex: 1, alignItems: "center", justifyContent: "center"}}>
          <Text>Carte</Text>
        </View>
        <MapView
          style={{flex: 5, width:375}}
          region={{
              latitude: this.state.lat,
              longitude: this.state.lng,
              latitudeDelta: 0.005,
              longitudeDelta: 0.005
          }}
          showsUserLocation = {true}
          scrollEnabled={true}
          liteMode={false}          
      >
        <MapView.Marker
          coordinate={{ latitude : this.state.lat , longitude : this.state.lng }}
        />
      </MapView>
			</View>
		)
	}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  text: {
  	color: "green",
  	fontSize: 20
  },
  button: {
  	borderWidth: 1,
  	borderColor: "black",
  	borderStyle: "solid",
  	margin: 15
  },
  map: {
        flex: 5,
        width: 350
      }
});

export default Map